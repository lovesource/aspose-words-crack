import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;

public class AsposeWords_19_1 {
    public static void main(String[] args) throws Exception {
        String fullPath = "/Users/liuzy/Downloads/aspose-words-19.1-jdk16.jar";
        ClassPool.getDefault().insertClassPath(fullPath);
        CtClass clazz = ClassPool.getDefault().getCtClass("com.aspose.words.zzZLJ");
        clazz.getDeclaredMethod("zzZI1").setBody("{return 1;}");
        clazz.getDeclaredMethod("zzZI0").setBody("{return 1;}");
        clazz.writeFile();
    }
}
