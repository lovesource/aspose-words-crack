import javassist.ClassPool;
import javassist.CtClass;

public class AsposeWords_19_5 {
    public static void main(String[] args) throws Exception {
        String fullPath = "/Users/liuzy/Downloads/aspose-words-19.5-jdk17.jar";
        ClassPool.getDefault().insertClassPath(fullPath);
        CtClass clazz = ClassPool.getDefault().getCtClass("com.aspose.words.zzZL8");
        clazz.getDeclaredMethod("zzZnM").setBody("{return 1;}");
        clazz.getDeclaredMethod("zzZnL").setBody("{return 1;}");
        clazz.writeFile();
    }
}
