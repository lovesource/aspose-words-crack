import com.aspose.words.Document;
import com.aspose.words.FontSettings;
import com.aspose.words.SaveFormat;

import java.io.FileInputStream;

public class WordPageCountTest {
    public static void main(String[] args) throws Exception {
        FontSettings.getDefaultInstance().setFontsFolder("/Volumes/Macintosh SD/code/aspose-words-crack/fonts", true);
        FontSettings.getDefaultInstance().setFontsFolder("/System/Library/Fonts", true);
        final String filePath = "/Users/liuzy/Documents/联合信签对外服务接口规范v0.4.3.docx";
        try (FileInputStream fis = new FileInputStream(filePath)) {
            Document doc = new Document(fis);
            System.out.println(doc.getPageCount());
            doc.save("a.pdf", SaveFormat.PDF);
            System.out.println("done");
        }
    }
}
