#!/bin/sh

set -e

# 官网下载
wget -c https://repository.aspose.com/repo/com/aspose/aspose-words/20.12/aspose-words-20.12-jdk17.jar

# 原文件不动,复制一份
\cp -f aspose-words-20.12-jdk17.jar aspose-words-20.12-jdk17-cracked.jar

# 用7z命令删除jar内的文件夹
7z d aspose-words-20.12-jdk17-cracked.jar META-INF

# 执行破解,生成类
javac AsposeWords_20_12.java -classpath .:javassist-3.24.1-GA.jar
java AsposeWords_20_12

# 用jar命令更新jar内的文件
jar uvf aspose-words-20.12-jdk17.jar com/aspose/words/zzZDZ.class

# 测试转换
